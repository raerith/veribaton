# Veribaton

Veribaton is a java Spring Boot service exposing a subset of [Openbaton](http://http://openbaton.github.io/) REST API to enable smart deployment through interaction with [Verifoo](https://github.com/netgroup-polito/verifoo).

## Usage
The service is available through its REST API. To interact with operations available and their documentation it is possible to access the following URL:

`http:[veribaton_hostbaton]:[veribaton_port]/swagger`


## Installation
Veribaton requires Java version 1.8 or higher, a release of Verifoo after January 2018 and Openbaton >= 5.0.0 to run properly.

##### Prerequisites
 - Download and install Java as described in Oracle Java [instruction page](https://www.java.com/en/download/help/download_options.xml)
 - Download and install the latest release of Verifoo from [here](https://github.com/netgroup-polito/verifoo)
 - Install Openbaton using [instructions](https://openbaton.github.io/documentation/nfvo-installation/)

##### Install from sources
- Clone this repository in a directory of choice:
```sh
$ git clone https://gitlab.com/raerith/veribaton/
```
- Modify application properties in folder resources using your favourite text editor.
```sh
$ cd veribaton/src/main/resources
$ vi application.properties
```
Editable properties:

| **Property**      | Description   | Default value|
| ------------- |:-------------:| -----:|
| server.port      | specifies the port the REST API server will listen on | 9090|
| verifoo.scheme      | scheme for verifoo URL, can be http or https      |   http |
| verifoo.host | Verifoo base URL, can be a hostname or IP address      |    localhost |
| verifoo.port | Port on which verifoo is listening | 8090 |
| verifoo.baseUri | Base URI for Verifoo REST service | /verifoo/rest |
| verifoo.deploymentUri | URI for deployment service on Verifoo REST API | /deployment |
| openbaton.host | Openbaton NFVO address, can be a hostname or IP | localhost |
| openbaton.port | Openbaton port | 8080 |
| openbaton.username | username to use when requesting services from Openbaton | admin|
| openbaton.password | password for Openbaton user specified | openbaton |
| openbaton.ssl | whether Openbaton REST uses https or not, can be true or false | false|

The property `spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration` can be deleted or commented out for enabling server basic authentication as provided by default from spring boot web configuration.

- Move to veribaton base directory and start the service using Gradle wrapper.
```sh
$ ./gradlew bootRun
```

## Packages description

#### it.polito.veribaton
The main class is `Application.java`. Takes care of starting up the tomcat embedded server and registering hooks for soft shutdown.
 
#### it.polito.veribaton.api.catalogue
Implements the API interface for Network Service Descriptor. The controller class is `NetworkServiceDescriptorController.java`, which handles requests for Network Services CRUD operations.

#### it.polito.veribaton.swagger
Includes the class `SwaggerController.java` which controls swagger documentation. Performs a URL rewrite to map /swagger endpoint to actual html location.

#### it.polito.veribaton.utils
The class `Converter.java` introduces methods to map Openbaton data model to Verifoo, and vice-versa.

Class `LogWriter` has utility methods to log XML ans JSON object to file.

#### it.polito.veribaton.errors
`VeribatonErrorController.java` is the request controller for errors happening in the REST interface, such as an invalid URL.

#### org.openbaton.catalogue
Defines Openbaton data model.

#### org.openbaton.exceptions
Includes all possible exceptions that could happen from the point of view of the NFVO.


